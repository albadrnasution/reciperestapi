This is an assignment for NNLife.
Submitted by:
  Albadr Nasution
  m.ln.albadr@gmail.com

-----

# Setup

There are two profiles in the web application: using database (mysql) or using map as mock.
If the profile used are the map (mock), data will disappear when application restarted.
Obviously, if the database profile are used, the data posted to the rest application
will be persistent in the mysql database.

Active profile are defined in the src/main/resources/application.properties. 
Select either of the following lines below.
```
spring.profiles.active=mysqldb
spring.profiles.active=mapimpl
```

JUnit tests of the RestController are using the `mapimpl` profile;

## Using MySql
When using mysql database, setup the mysql database first by executing the following sql file
into your database.
```
sql/db.setup.sql
```
Set the database username=`root` and password=`rootPassword`;
If you use any other previleges information, change the information in ConfigUsingMysqlDb.java. 

# Compiling Running the Web Application

1. Execute the following code in terminal to install the web application.
This will also run the junit test using the `mapimpl` profiles.
```
mvn clean install
```


2. Execute the following code in terminal to run the web application;
```
java -jar target/nnlife-recipe-rest-0.1.0.war
```

# Beanstaclk URL for this project
http://RecipeRest-env.r2wpqsn2mg.ap-northeast-1.elasticbeanstalk.com

