package app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.filter.CharacterEncodingFilter;

import app.recipe.RecipeDao;
import app.recipe.RecipeDaoDbImpl;

@Configuration
@Profile("mysqldb")
public class ConfigUsingMysqlDb {

	@Bean
	public Connection connection() throws SQLException {
		String url = "jdbc:mysql://aa1rrqlxioyr600.cjqxtbp8gfzz.ap-northeast-1.rds.amazonaws.com:3306/nnlife?&serverTimezone=Japan";
		String username = "root";
		String password = "rootPassword";
		return DriverManager.getConnection(url, username, password);
	}

	@Bean
	public RecipeDao productDaoDb(Connection conn) {
		return new RecipeDaoDbImpl(conn);
	}

	CharacterEncodingFilter characterEncodingFilter() {
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
		filter.setEncoding("UTF-8");
		filter.setForceEncoding(true);
		return filter;
	}
}
