package app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import app.recipe.RecipeDao;
import app.recipe.RecipeDaoMapImpl;

@Configuration
@Profile("mapimpl")
public class ConfigUsingMapImpl {
	@Bean
	public RecipeDao productDao() {
		return new RecipeDaoMapImpl();
	}

}
