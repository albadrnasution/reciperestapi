package app.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import app.controller.error.InsertFailedException;
import app.controller.response.DeleteResponse;
import app.controller.response.RecipeResponse;
import app.controller.response.RecipesResponse;
import app.recipe.Recipe;
import app.recipe.RecipeDao;

@RestController
public class RecipeController {

	private final RecipeDao dao;

	@Autowired
	public RecipeController(RecipeDao dao) {
		this.dao = dao;
	}

	@RequestMapping(value = "/recipes/{id}", method = RequestMethod.GET)
	public RecipeResponse get(@PathVariable("id") int id) {
		Recipe recipe = dao.get(id);
		if (Objects.isNull(recipe)) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		String message = "Recipe details by id";
		return new RecipeResponse(message, recipe);
	}

	@RequestMapping(value = "/recipes", method = RequestMethod.GET)
	public RecipesResponse getList() {
		List<Recipe> result = dao.getAll();
		return new RecipesResponse(result);
	}

	@RequestMapping(value = "/recipes", method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
			consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<RecipeResponse> add(@RequestBody Recipe newRecipe)
			throws InsertFailedException {
		long id = dao.save(newRecipe);
		if (id < 0) {
			throw new InsertFailedException();
		}

		String location = "/recipes/" + id;
		String message = "Recipe successfully created!";
		RecipeResponse successResponse = new RecipeResponse(message, newRecipe);
		return ResponseEntity.ok()
				.body(successResponse);
	}

	@RequestMapping(value = "/recipes/{id}", method = RequestMethod.PATCH,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
			consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<RecipeResponse> update(@PathVariable("id") int id,
			@RequestBody Recipe patch) throws InsertFailedException {
		Recipe updatedRecipe = dao.update(id, patch);
		if (Objects.isNull(updatedRecipe)) {
			throw new InsertFailedException();
		}
		String location = "/recipes/" + id;
		String message = "Recipe successfully updated!";
		RecipeResponse successResponse = new RecipeResponse(message,
				updatedRecipe);
		return ResponseEntity.ok()
				.body(successResponse);
	}

	@RequestMapping(value = "/recipes/{id}", method = RequestMethod.DELETE)
	public DeleteResponse delete(@PathVariable("id") int id) {
		boolean success = dao.delete(id);
		if (!success) {
			return new DeleteResponse("No Recipe found");
		}
		return new DeleteResponse("Recipe successfully removed!");
	}
}
