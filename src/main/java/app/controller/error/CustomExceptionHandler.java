package app.controller.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CustomExceptionHandler {

	@ExceptionHandler(InsertFailedException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public InsertFailedResponse handleException(InsertFailedException ex) {

		InsertFailedResponse errorResponse = 
				new InsertFailedResponse("Recipe creation failed!", 
						"title, making_time, serves, ingredients, cost");
		return errorResponse;
	}
}
