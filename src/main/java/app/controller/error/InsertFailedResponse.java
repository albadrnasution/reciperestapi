package app.controller.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class InsertFailedResponse {
	String message;
	String required;
}
