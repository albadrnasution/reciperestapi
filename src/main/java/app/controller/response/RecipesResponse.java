package app.controller.response;

import java.util.ArrayList;
import java.util.List;

import app.recipe.Recipe;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class RecipesResponse {
	List<Recipe> recipes;
	
	public RecipesResponse(Recipe aRecipe) {
		this.recipes = new ArrayList<Recipe>();
		this.recipes.add(aRecipe);
	}
}
