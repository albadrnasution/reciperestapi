package app.controller.response;

import java.util.ArrayList;
import java.util.List;

import app.recipe.Recipe;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class RecipeResponse {
	String message;
	List<Recipe> recipe;
	
	public RecipeResponse(String message, Recipe aRecipe) {
		this.message = message;
		this.recipe = new ArrayList<Recipe>();
		this.recipe.add(aRecipe);
	}
}
