package app.recipe;

import java.util.List;

public interface RecipeDao {

	Recipe get(int id);

	List<Recipe> getAll();

	boolean exist(int id);

	long save(Recipe recipe);
	
	Recipe update(int id, Recipe recipe);

	boolean delete(int id);

}
