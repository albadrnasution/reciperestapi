package app.recipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class RecipeDaoMapImpl implements RecipeDao {

	private final AtomicInteger counter = new AtomicInteger();
	Map<Integer, Recipe> map = new HashMap<>();

	public RecipeDaoMapImpl() {
	}

	@Override
	public Recipe get(int id) {
		if (map.containsKey(id)) {
			return map.get(id);
		}
		return null;
	}

	@Override
	public List<Recipe> getAll() {
		List<Recipe> result = new ArrayList<>();
		for (Entry<Integer, Recipe> entry : map.entrySet()) {
			result.add(entry.getValue());
		}
		return result;
	}

	@Override
	public boolean exist(int id) {
		return map.containsKey(id);
	}

	@Override
	public long save(Recipe product) {
		int id = counter.incrementAndGet();
		map.put(id, product);
		return id;
	}

	@Override
	public Recipe update(int id, Recipe recipe) {
		map.put(id, recipe);
		return get(id);
	}

	@Override
	public boolean delete(int id) {
		Recipe recipe = map.remove(id);
		return Objects.nonNull(recipe);
	}

	private volatile static RecipeDaoMapImpl instance;

	public static RecipeDaoMapImpl getInstance() {
		if (instance == null) {
			synchronized (RecipeDaoMapImpl.class) {
				if (instance == null) {
					instance = new RecipeDaoMapImpl();
				}
			}
		}
		return instance;
	}
}
