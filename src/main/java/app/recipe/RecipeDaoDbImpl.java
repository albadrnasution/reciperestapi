package app.recipe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class RecipeDaoDbImpl implements RecipeDao {

	private final Connection conn;

	private static final String GET = "SELECT title, making_time, serves, ingredients, cost "
			+ "FROM recipes WHERE id = ?";
	private static final String GET_ALL = "SELECT title, making_time, serves, ingredients, cost "
			+ "FROM recipes";
	private static final String EXIST = "SELECT COUNT(*) count FROM recipes WHERE id = ?";
	private static final String INSERT = "INSERT INTO recipes "
			+ "(title, making_time, serves, ingredients, cost, created_at, updated_at)"
			+ "VALUES (?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
	private static final String UPDATE = "UPDATE recipes "
			+ "SET title = ?, making_time = ?, serves = ?, ingredients = ?, "
			+ "cost = ?, updated_at = CURRENT_TIMESTAMP WHERE id = ?";
	private static final String DELETE = "DELETE FROM recipes WHERE id = ?";

	@Autowired
	public RecipeDaoDbImpl(Connection conn) {
		this.conn = conn;
	}

	@Override
	public Recipe get(int id) {
		Recipe result = null;
		try (PreparedStatement ps = conn.prepareStatement(GET)) {
			ps.setLong(1, id);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					result = extractRecipe(rs);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Recipe> getAll() {
		List<Recipe> result = new ArrayList<>();
		try (PreparedStatement ps = conn.prepareStatement(GET_ALL)) {
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					Recipe recipe = extractRecipe(rs);
					result.add(recipe);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	private Recipe extractRecipe(ResultSet rs) throws SQLException {
		String title = rs.getString("title");
		String makingTime = rs.getString("making_time");
		String serves = rs.getString("serves");
		String ingredients = rs.getString("ingredients");
		int cost = rs.getInt("cost");
		return new Recipe(title, makingTime, serves, ingredients, cost);
	}

	@Override
	public boolean exist(int id) {
		boolean result = false;
		try (PreparedStatement ps = conn.prepareStatement(EXIST)) {
			ps.setInt(1, id);
			ps.execute();
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					result = rs.getInt("count") > 0;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public long save(Recipe recipe) {
		long id = -1;

		try (PreparedStatement ps = conn.prepareStatement(INSERT,
				Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, recipe.getTitle());
			ps.setString(2, recipe.getMakingTime());
			ps.setString(3, recipe.getServes());
			ps.setString(4, recipe.getIngredients());
			ps.setInt(5, recipe.getCost());
			ps.executeUpdate();
			try (ResultSet rs = ps.getGeneratedKeys()) {
				if (rs.next()) {
					id = rs.getLong(1);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}

	@Override
	public Recipe update(int id, Recipe recipe) {
		try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
			ps.setString(1, recipe.getTitle());
			ps.setString(2, recipe.getMakingTime());
			ps.setString(3, recipe.getServes());
			ps.setString(4, recipe.getIngredients());
			ps.setInt(5, recipe.getCost());
			ps.setInt(6, id);
			ps.executeUpdate();

			return get(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
			ps.setInt(1, id);
			return ps.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
