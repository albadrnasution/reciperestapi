package app.recipe;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Recipe {
//	private long id;
	private String title;
	private String makingTime;
	private String serves;
	private String ingredients;
	private int cost;
//	private LocalDate createdAt;
//	private LocalDate updatedAt;

//	protected void setId(long id) {
//		this.id = id;
//	}
}
